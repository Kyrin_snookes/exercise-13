﻿using System;

namespace exercise_13
{
    class Program
    {
        static void Main(string[] args)
        {
           //Start the program with Clear();
           Console.Clear();
           
           var name= "kyrin";
           
           System.Console.WriteLine($"My name is {name}");

           
           //End the program with blank line and instructions
           Console.ResetColor();
           Console.WriteLine();
           Console.WriteLine("Press <Enter> to quit the program");
           Console.ReadKey();
        }
    }
}
